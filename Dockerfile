FROM node:latest

ARG CI_COMMIT_SHORT_SHA=latest
ARG CI_COMMIT_REF_NAME

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
RUN mv package.json _package.json